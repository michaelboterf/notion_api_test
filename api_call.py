import os
import requests

# Constants
NOTION_SECRET_API_TOKEN = os.environ.get('NOTION_TOKEN')
NOTION_PARENT_PAGE_ID = "441046b33c854abc9805734cbe7cbdde"  # ID of the "Repo Documentation" page

BASE_URL = "https://api.notion.com/v1/pages"
HEADERS = {
    "Authorization": f"Bearer {NOTION_SECRET_API_TOKEN}",
    "Content-Type": "application/json",
    "Notion-Version": "2021-05-13"  # This may change; always refer to official Notion API documentation
}


def create_notion_page(title, content):
    # Break content into chunks of 2000 characters
    CHUNK_SIZE = 2000
    content_chunks = [content[i:i+CHUNK_SIZE] for i in range(0, len(content), CHUNK_SIZE)]

    children_blocks = []
    for chunk in content_chunks:
        block = {
            "object": "block",
            "type": "paragraph",
            "paragraph": {
                "text": [
                    {
                        "type": "text",
                        "text": {
                            "content": chunk
                        }
                    }
                ]
            }
        }
        children_blocks.append(block)

    data = {
        "parent": {"type": "page_id", "page_id": NOTION_PARENT_PAGE_ID},
        "properties": {
            "title": [
                {
                    "type": "text",
                    "text": {
                        "content": title
                    }
                }
            ]
        },
        "children": children_blocks
    }

    response = requests.post(BASE_URL, headers=HEADERS, json=data)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error creating page for {title}. Status Code: {response.status_code}. Message: {response.text}")
        return None


if __name__ == "__main__":
    # Loop through all .md files in the current directory
    for filename in os.listdir():
        if filename.endswith(".md"):
            with open(filename, 'r') as file:
                content = file.read()
                title = filename.replace('.md', '')
                response = create_notion_page(title, content)
                if response:
                    print(f"Created page for {filename}: {response.get('url')}")
                else:
                    print(f"Failed to create page for {filename}")
