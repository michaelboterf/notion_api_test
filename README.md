# Repo Documentation to Notion

A simple Python script to upload all Markdown (`.md`) files in the current directory to a specific Notion page under the "Repo Documentation" section.

## Prerequisites

1. **Python 3**: The script is written in Python 3. Ensure you have it installed and accessible from your command line.

2. **Notion Account**: You should have a Notion account and a page (e.g., "Repo Documentation") where you want the Markdown files to be uploaded.

3. **Notion API Integration Token**: You need to create an integration within Notion to get the API token. Follow these steps:
    1. Go to [https://www.notion.so/my-integrations](https://www.notion.so/my-integrations) to create a new integration and obtain your `NOTION_SECRET_API_TOKEN`.
    1. Assign this integration to a workspace.
    1. Navigate to the Notion page where you want the Markdown files from this repository to be uploaded.
    1. Click on the `•••` menu (ellipsis or more options) at the top right of the page.
    1. Scroll down and select the `Add connections` option.
    1. In the search bar that appears, start typing the name of the integration you just created.
    1. When you see your integration in the dropdown list, click on it. This action shares the page with your integration.
    1. To verify, click on the `•••` menu again. You should see your integration listed under a section "Connections" or similar.

For a more detailed explanation of integration permissions, see the official [Notion Developers' documentation](https://developers.notion.com/docs/authorization#integration-permissions).

4. **Notion Page ID**:
    1. Open the Notion page where you want to add the Markdown content.
    2. Look at the URL. It will be of the format: `https://www.notion.so/WorkspaceName/PageName-abcdefgh12345678ijklmnop`.
    3. The string after the last hyphen (`-`) is the page ID. Copy this for use in the script.

## Setup

1. **Clone this repository**:
    ```bash
    git clone [repository_url]
    ```

2. **Install required Python packages**:
    ```bash
    pip install -r requirements.txt
    ```

3. **Set Up Your Environment Variable**:
    - **For bash**:
        Open `~/.bashrc` and add the following:
        ```bash
        export NOTION_TOKEN="YOUR_NOTION_SECRET_API_TOKEN"
        ```
        Save the file and source it:
        ```bash
        source ~/.bashrc
        ```

    - **For zsh**:
        Open `~/.zshrc` and add the following:
        ```bash
        export NOTION_TOKEN="YOUR_NOTION_SECRET_API_TOKEN"
        ```
        Save the file and source it:
        ```bash
        source ~/.zshrc
        ```

## Usage

1. Place the script in the directory containing the Markdown files you wish to upload.
2. Run the script:
    ```bash
    python script_name.py
    ```

The script will loop through all `.md` files in the current directory and create a new page for each file under the "Repo Documentation" section in Notion.

### TODO:

maybe make this use graphql to pull all gitlab repos down and publish them to Notion, we'd have to destroy and recreate as necessary, but ideally this would happen with a pipeline/gitlab or github acitons
